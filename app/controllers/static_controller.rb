class StaticController < ApplicationController
  def home
    products = Product.all
    @memorys = products.uniq.pluck(:memory)
    @memorys = @memorys.reject { |m| m.nil? }.sort

    @brands = products.uniq.pluck(:brand)
    @brands = @brands.reject { |b| b.nil? }.sort

    @c_brands = products.uniq.pluck(:chip_brand)
    @c_brands = @c_brands.reject { |c| c.nil? }.sort
  end

  def search
    @memorys = Product.uniq.pluck(:memory)
    @memorys = @memorys.reject { |m| m.nil? }.sort

    @brands = Product.uniq.pluck(:brand) 
    @brands = @brands.reject { |b| b.nil? }.sort

    @c_brands = Product.uniq.pluck(:chip_brand)
    @c_brands = @c_brands.reject { |c| c.nil? }.sort

    @products = Product.all
    @products = @products.where(memory: params['filter']['mem']) unless params['filter']['mem'].count == 1
    @products = @products.where(brand: params['filter']['brand']) unless params['filter']['brand'].count == 1
    @products = @products.where(chip_brand: params['filter']['c_brand']) unless params['filter']['c_brand'].count == 1
  end

  def compare
    @title = "TiTle"
  end
end
