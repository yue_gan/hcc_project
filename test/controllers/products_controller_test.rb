require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  setup do
    @product = products(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product" do
    assert_difference('Product.count') do
      post :create, product: { brand: @product.brand, chip_brand: @product.chip_brand, m_speed: @product.m_speed, memory: @product.memory, model: @product.model, p_speed: @product.p_speed, price_amazon: @product.price_amazon, price_bestbuy: @product.price_bestbuy, price_newegg: @product.price_newegg, title: @product.title }
    end

    assert_redirected_to product_path(assigns(:product))
  end

  test "should show product" do
    get :show, id: @product
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product
    assert_response :success
  end

  test "should update product" do
    patch :update, id: @product, product: { brand: @product.brand, chip_brand: @product.chip_brand, m_speed: @product.m_speed, memory: @product.memory, model: @product.model, p_speed: @product.p_speed, price_amazon: @product.price_amazon, price_bestbuy: @product.price_bestbuy, price_newegg: @product.price_newegg, title: @product.title }
    assert_redirected_to product_path(assigns(:product))
  end

  test "should destroy product" do
    assert_difference('Product.count', -1) do
      delete :destroy, id: @product
    end

    assert_redirected_to products_path
  end
end
